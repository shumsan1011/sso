var Hapi = require('hapi');
var Path = require('path');
var Vision = require('vision');
var Inert = require('inert');
var Boom = require('boom');
var Handlebars = require('handlebars');
var server = new Hapi.Server();

server.connection({
  port: 7001,
  host: '0.0.0.0'
});

server.register(Inert, () => {});
server.register(Vision, function (err) {  
  if (err) {
    console.log('Cannot register vision')
  }

  server.views({
    engines: {
      html: Handlebars
    },
    path: __dirname + '/views',
    layout: false
  });
});

// Render form
server.route({
  method: 'GET',
  path: '/{param*}',
  config: {
    handler: function (request, reply) {
      reply.view('index');
    }
  }
});

server.start(function () {
    console.log('Up');
});

module.exports = server;