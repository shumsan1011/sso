'use strict';

const Hapi = require('hapi');
const appPlugin = require('../app');

const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();

const describe = lab.describe;
const it = lab.it;
const before = lab.before;
const expect = Code.expect;

describe('Account Server application management', () => {
  let server;

  before((done) => {
    const plugins = [appPlugin];
    server = new Hapi.Server();
    server.connection({ port: 8000 });
    server.register(plugins, (err) => {
      if (err) {
        return done(err);
      }

      server.initialize(done);
    });
  });

  it('Fetches list of SSO applications', (done) => {
    server.inject({
      method: 'GET',
      url: '/management/applications',
    }, response => {
      expect(response.statusCode).to.equal(200);
      expect(response.result.length).to.equal(1);
      
      done();
    });
  });
  

});
