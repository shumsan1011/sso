'use strict';

const Hapi = require('hapi');
const appPlugin = require('../app');

const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();

const describe = lab.describe;
const it = lab.it;
const before = lab.before;
const expect = Code.expect;

describe('Account Server SSO authentication', () => {
  let server;

  before((done) => {
    const plugins = [appPlugin];
    server = new Hapi.Server();
    server.connection({ port: 8000 });
    server.register(plugins, (err) => {
      if (err) {
        return done(err);
      }

      server.initialize(done);
    });
  });

  it('Authenticates with valid credentials', done => {
    server.inject({
      method: 'POST',
      url: '/auth',
      payload: {
        applicationId: 'testapp',
        username: 'user1@example.com',
        password: '123456'
      }
    }, response => {
      expect(response.statusCode).to.equal(200);
      done();
    });
  });

  it('Fails to authenticate with invalid credentials', done => {
    server.inject({
      method: 'POST',
      url: '/auth',
      payload: {
        applicationId: 'testapp',
        username: 'user100@example.com',
        password: '123456'
      }
    }, response => {
      expect(response.statusCode).to.equal(401);
      done();
    });
  });
  
  it('Fails to authenticate with invalid application name', done => {
    server.inject({
      method: 'POST',
      url: '/auth',
      payload: {
        applicationId: 'testapp1000',
        username: 'user1@example.com',
        password: '123456'
      }
    }, response => {
      expect(response.statusCode).to.equal(401);
      done();
    });
  });
});
