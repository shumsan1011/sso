'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('UsersToApplications', [{
      id: 1,
      userId: 1,
      applicationId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      userId: 2,
      applicationId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 3,
      userId: 1,
      applicationId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 4,
      userId: 2,
      applicationId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('UsersToApplications', null, {});
  }
};
