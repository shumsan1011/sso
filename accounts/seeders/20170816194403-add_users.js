'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Users', [{
      id: 1,
      username: 'user1',
      email: 'user1@example.com',
      password: '$2a$10$1YHm2KsBbMbCZEA5fT1giegE0WXSLQ4MKXJVKMA2Ti2H2SbuAA24G',
      enabled: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      username: 'user2',
      email: 'user2@example.com',
      password: '$2a$10$1YHm2KsBbMbCZEA5fT1giegE0WXSLQ4MKXJVKMA2Ti2H2SbuAA24G',
      enabled: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
