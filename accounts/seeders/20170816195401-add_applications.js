'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Applications', [{
      id: 1,
      privateKey: '$2a$10$1YHm2KsBbMbCZEA5fT1giee3a$10$.Ni0t2v2qu7GkOAIB4aXWeAba$V3',
      textualId: 'testapp1',
      endpointUrl: 'https://app1.taralite.alexshumilov.ru',
      enabled: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      privateKey: '$2a$10$1YHm2KsBbMbCZEA5fT1giee3a$10$.Ni0t2v2qu7GkOAIB4aXWeAba$V3',
      textualId: 'testapp2',
      endpointUrl: 'https://app2.taralite.alexshumilov.ru',
      enabled: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Applications', null, {});
  }
};
