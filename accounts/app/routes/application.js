'use strict';

const appplicationController = require('../controllers/application');

module.exports = [{
  method: 'GET',
  path: '/management/applications',
  handler: appplicationController.getAll
}];
