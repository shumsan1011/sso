'use strict';

const Joi = require('joi');
const webappAuthenticationController = require('../../controllers/webappAuth');

module.exports = [{
  method: 'POST',
  path: '/authenticate/webapp',
  config: {
    validate: {
      payload: {
        username: Joi.string().email().required(),
        application: Joi.string().required(),
        password: Joi.string().min(6).max(200).required()
      }
    },
    handler: webappAuthenticationController.authenticate
  }
}, {
  method: 'POST',
  path: '/authenticate/webapp/refresh',
  config: {
    validate: {
      payload: {
        refreshToken: Joi.string().required()
      }
    },
    handler: webappAuthenticationController.refresh
  }
}, {
  method: 'GET',
  path: '/authenticate/webapp/signout',
  config: {
    handler: webappAuthenticationController.signout
  }
}];
