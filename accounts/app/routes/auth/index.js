'use strict';

const spaRoutes = require('./spa');
const webappRoutes = require('./webapp');

var result = [];
result = result.concat(spaRoutes, webappRoutes);

module.exports = result;