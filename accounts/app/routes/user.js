'use strict';

const userController = require('../controllers/user');

module.exports = [{
  method: 'GET',
  path: '/management/users',
  handler: userController.getAll
}];
