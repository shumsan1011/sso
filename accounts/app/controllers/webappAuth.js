'use strict';

const model = require('../models');
const userService = require('../services/user');
const applicationService = require('../services/application');
const Boom = require('boom');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const forceHttpsForClients = true;

/**
 * Extracts the application URL - the protocol, host name and the port.
 */
const extractApplicationUrl = (url) => {
  let hostname;

  let protocol = false;
  if (url.indexOf("://") > -1) {
    protocol = url.split('://')[0];
    hostname = url.split('/')[2];
  } else {
    hostname = url.split('/')[0];
  }

  if (forceHttpsForClients) {
    hostname = 'https://' + hostname;
  } else {
    hostname = protocol + '://' + hostname;
  }

  hostname = hostname.split('?')[0];
  return hostname;
};

/**
 * Tries to authenticate user.
 */
const authenticateUser = (user, applicationPrivateKey, password) => {
  const result = new Promise((resolve, reject) => {
    let saltedPassword = bcrypt.hashSync(password, process.env.PASSWORD_SALT_SECRET);
    if (saltedPassword === user.dataValues.password) {
      
      // @todo Put the refresh token to the Redis
      // @todo Storing private keys for applications is not secure (consider that the database will be stolen)

      const accessToken = jwt.sign({
        id: user.id,
        username: user.username,
        email: user.email
      }, applicationPrivateKey);

      const refreshToken = null;

      resolve({accessToken, refreshToken});
    } else {
      reject('Invalid username or password');
    }
  });

  return result;
};

/**
 * Authenticates user.
 */
module.exports.authenticate = (request, reply) => {
  applicationService.getByTextualId(request.payload.applicationId).then((application) => {
    if (application) {
      // @todo users-to-application access
      userService.getByEmail(request.payload.username).then((user) => {
        if (user) {
          authenticateUser(user, application.dataValues.privateKey, request.payload.password).then((tokens) => {
            reply(tokens);
          }).catch((errorMessage) => {
            reply(Boom.unauthorized(errorMessage));
          });
        } else {
          reply(Boom.unauthorized('Invalid username or password'));
        }
      });
    } else {
      reply(Boom.unauthorized('Invalid application identifier'));
    }
  });
};

/**
 * Authenticating using the sing in form.
 */
module.exports.signInFromForm = (request, reply) => {
  const payload = request.payload;

  if (payload.continue) {
    let cleanedApplicationUrl = extractApplicationUrl(payload.continue);
    applicationService.getByApplicationUrl(cleanedApplicationUrl).then((application) => {
      if (application) {
        // @todo users-to-application access
        userService.getByEmail(payload.email).then((user) => {
          if (user) {
            authenticateUser(user, application.dataValues.privateKey, payload.password).then((tokens) => {
              let data = Object.assign({}, tokens, {
                continueUrl: payload.continue,
                applicationTextualId: application.dataValues.textualId
              });
              reply.view('successAuth', data);
            }).catch((errorMessage) => {
              reply.view('index', { error: errorMessage, email: payload.email });
            });
          } else {
            reply.view('index', { error: 'Invalid username or password', email: payload.email });
          }
        });
      } else {
        reply.view('index', { error: 'Invalid application', email: payload.email });
      }
    });
  } else {
    reply.view('index', { error: 'Invalid request', email: payload.email });
  }
};

/**
 * Sign out current user.
 */
module.exports.signOut = (request, reply) => {
  // @todo destroy refresh tokens in Redis

  const payload = request.query;
  if (payload.continue) {
    let cleanedApplicationUrl = extractApplicationUrl(payload.continue);
    applicationService.getByApplicationUrl(cleanedApplicationUrl).then((application) => {
      if (application) {
        let result = false;
        for (let entry in request.state) {
          if (entry === ('taralite_' + application.dataValues.textualId)) {
            result = {};
            result.destroyedCookieName = entry;
            break;
          }
        }
        
        if (result) {
          reply.view('discardAuth', result);
        } else {
          reply(Boom.badRequest('Unable to perform request'));
        }
      } else {
        reply(Boom.badRequest('Invalid application'));
      }
    });
  } else {
    reply(Boom.badRequest('Invalid application'));
  }
};

/**
 * Displays sign in form.
 */
module.exports.renderAuthenticationForm = (request, reply) => {
  let params = request.query;
  if (params.action && params.action === 'signout') {
    reply.view('discardAuth');
  } else {
    if (request.state.taralitetoken) {

      // @todo
      let tokenIsValid = true;

      if (tokenIsValid) {
        if ("checkAuth" in params) {
          reply.view('checkAuth', { auth: true, token: request.state.taralitetoken });
        } else {
          reply.view('successAuth', { token: request.state.taralitetoken });
        }
      } else {
        reply.view('index');
      }
    } else {
      if ("checkAuth" in params) {
        reply.view('checkAuth', { auth: false });
      } else {
        reply.view('index');
      }
    }
  }
};

module.exports.renderSignUpForm = (request, reply) => {
  reply();
};



