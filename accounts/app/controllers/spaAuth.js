'use strict';

const model = require('../models');
const userService = require('../services/user');
const applicationService = require('../services/application');
const Boom = require('boom');
const jwt = require('jsonwebtoken');
const bluebird = require('bluebird');
const constants = require('./../constants');
const bcrypt = require('bcrypt');
const redisClient = require('./../services/redis');

/**
 * Tries to authenticate user.
 */
const authenticateUser = (user, password, application) => {
  const result = new Promise((resolve, reject) => {
    let saltedPassword = bcrypt.hashSync(password, process.env.PASSWORD_SALT_SECRET);
    // Checking if password is correct
    if (saltedPassword === user.dataValues.password) {
      // Setting the access_token lifetime in seconds
      const accessTokenExpiration = 60;

      // Setting the refresh_token lifetime in days
      const refreshTokenExpiration = 10;
      
      // Setting the acceptance_token lifetime in days
      const acceptanceTokenExpiration = 10;

      const accessToken = jwt.sign({
        id: user.id,
        username: user.username,
        email: user.email,
        type: constants.TOKEN_TYPE.ACCESS,
        exp: Math.floor(Date.now() / 1000) + (accessTokenExpiration),
      }, application.dataValues.privateKey);

      const refreshToken = jwt.sign({
        id: user.id,
        type: constants.TOKEN_TYPE.REFRESH,
        exp: Math.floor(Date.now() / 1000) + (refreshTokenExpiration * 24 * 60 * 60),
      }, application.dataValues.privateKey);

      // Storing refresh token in Redis
      redisClient.lpushAsync('userId_' + user.id + '_refresh_tokens', [refreshToken]).then((res) => {
        // Generating the acceptance token
        let acceptanceToken = jwt.sign({
          id: user.id,
          type: constants.TOKEN_TYPE.ACCEPTANCE,
          exp: Math.floor(Date.now() / 1000) + (acceptanceTokenExpiration * 24 * 60 * 60),
        }, process.env.JWT_SECRET);

        resolve({accessToken, refreshToken, acceptanceToken});
      }).catch((err) => {
        console.log(err);
      });
    } else {
      reject('Invalid username or password');
    }
  });

  return result;
};

/**
 * Authenticates user.
 */
module.exports.authenticate = (request, reply) => {
  applicationService.getByTextualId(request.payload.application).then((application) => {
    if (application) {
      userService.getByEmail(request.payload.username).then((user) => {
        if (user) {
          authenticateUser(user, request.payload.password, application)
          .then(({accessToken, refreshToken, acceptanceToken}) => {
            let cookieOptions = {
              ttl: 365 * 24 * 60 * 60 * 1000,
              encoding: 'none',
              isSecure: true,
              isHttpOnly: true,
              strictHeader: true
            };

            reply({accessToken, refreshToken}).state('taraliteSSOToken', accessToken, cookieOptions);
          }).catch((errorMessage) => {
            reply(Boom.unauthorized(errorMessage));
          });
        } else {
          reply(Boom.unauthorized('Invalid username or password'));
        }
      });
    } else {
      reply(Boom.unauthorized('Invalid application identifier'));
    }
  });
};


/**
 * Signs out user.
 */
module.exports.signout = (request, reply) => {

};


/**
 * Refreshes the token for user.
 */
module.exports.refresh = (request, reply) => {

};