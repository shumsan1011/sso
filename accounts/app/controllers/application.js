'use strict';

const applicationService = require('../services/application');
const Boom = require('boom');

module.exports.getAll = (request, reply) => {
  applicationService.getAll().then((data) => {
    let result = [];
    data.map((item) => {
      result.push({
        id: item.id,
        textualId: item.textualId,
        endpointUrl: item.endpointUrl,
        enabled: item.enabled,
        createdAt: item.createdAt
      });
    });

    reply(result);
  });
};

