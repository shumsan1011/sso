'use strict';

const userService = require('../services/user');
const Boom = require('boom');

module.exports.getAll = (request, reply) => {
  userService.getAll().then((data) => {
    let result = [];
    data.map((item) => {
      result.push({
        id: item.id,
        username: item.username,
        enabled: item.enabled,
        createdAt: item.createdAt
      });
    });

    reply(result);
  });
};

