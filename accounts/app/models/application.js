'use strict';

module.exports = function(sequelize, DataTypes) {
  var Application = sequelize.define("Applications", {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      autoIncrement: true,
      unique: true
    },
    privateKey: {
      type: DataTypes.STRING,
      allowNull: false
    },
    textualId: {
      type: DataTypes.STRING,
      unique: true
    },
    endpointUrl: DataTypes.STRING,
    enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {
    freezeTableName: true
  });

  Application.associate = (models) => {
    Application.hasMany(models.UsersToApplications, { foreignKey: 'applicationId' });
  };
  
  return Application;
};
