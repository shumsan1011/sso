'use strict';

module.exports = function(sequelize, DataTypes) {
  var UsersToApplications = sequelize.define("UsersToApplications", {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      autoIncrement: true,
      unique: true
    },
    userId: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    applicationId: {
      allowNull: false,
      type: DataTypes.INTEGER
    }
  }, {
    freezeTableName: true
  });
  
  UsersToApplications.associate = (models) => {
    UsersToApplications.belongsTo(models.Users, { foreignKey: 'userId' });
    UsersToApplications.belongsTo(models.Applications, { foreignKey: 'applicationId' });
  };

  return UsersToApplications;
};
