'use strict';

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("Users", {
    id: {
        primaryKey: true,
        type: DataTypes.UUID,
        autoIncrement: true,
        unique: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: true
    },
    password: DataTypes.STRING,
    enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {
    freezeTableName: true
  });
  
  User.associate = (models) => {
    User.hasMany(models.UsersToApplications, { foreignKey: 'userId' });
  };
  
  return User;
};
