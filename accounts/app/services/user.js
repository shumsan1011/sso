'use strict';

const Model = require('../models');

module.exports.getByEmail = function (email) {
  return Model.Users.findOne({
    where: { email },
    include: [{
      model: Model.UsersToApplications
    }]
  });
};


module.exports.getAll = function () {
  return Model.Users.findAll();
};