'use strict';

const Model = require('../models');

module.exports.getByTextualId = (textualId) => {
  return Model.Applications.findOne({
    where: { textualId }
  });
};

module.exports.getByApplicationUrl = (applicationUrl) => {
  return Model.Applications.findOne({
    where: { endpointUrl: applicationUrl }
  });
};

module.exports.getAll = (options) => {
  return Model.Applications.findAll(options);
}; 