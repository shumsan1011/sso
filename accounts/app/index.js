'use strict';

require('dotenv').config();

const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const applicationRoutes = require('./routes/application');

exports.register = (plugin, options, next) => {
  // external authentication routes
  plugin.route(authRoutes);

  // management routes
  plugin.route(userRoutes);
  plugin.route(applicationRoutes);

  next();
};

exports.register.attributes = {
  name: 'app'
};
