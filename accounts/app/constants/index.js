'use strict';

/**
 * Defining application-wide constants.
 */

module.exports = {
  TOKEN_TYPE: {
    ACCESS: 1,
    REFRESH: 2,
    ACCEPTANCE: 3,
  }
};