var Handlebars = require('handlebars');
var Vision = require('vision');
var visionary = require('visionary');

const envKey = key => {
  const configuration = {
    host: process.env.NODE_HOST || 'localhost',
    port: process.env.NODE_PORT || 8000,
    version: process.env.API_VERSION || '0.0.1'
  };

  return configuration[key];
};

const manifest = {
  connections: [{
    host: envKey('host'),
    port: envKey('port'),
    routes: {
      cors: true
    },
    router: {
      stripTrailingSlash: true
    }
  }],
  registrations: [
    {
      plugin: 'hapi-auth-jwt2'
    }, {
      plugin: './app/plugins/auth'
    }, {
      plugin: './app'
    }, {
      plugin: 'vision'
    }, {
      plugin: {
        register: "visionary",
        options: {
          engines: {
            html: "handlebars"
          },
          path: __dirname + '/../app/views'
        }
      }
    }, {
      plugin: 'inert'
    }, {
      plugin: {
        register: 'good',
        options: {
          ops: { interval: 1000 },
          reporters: {
            console: [
              {
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ log: '*', response: '*', error: '*' }]
              }, {
                module: 'good-console'
              }, 'stdout'
            ]
          }
        }
      }
    }
  ]
};

module.exports = manifest;
